# vue-nuxt-static

> Nuxt.js project

> Learn from : [Static Site Generators: Nuxt.js](https://medium.com/a-man-with-no-server/static-site-generators-nuxt-js-2fa9782d27c8)

> Date : 2018-06-13

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
